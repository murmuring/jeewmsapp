package com.jeewms.www.wms.util;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.jeewms.www.wms.ui.view.NetworkImageHolderView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 13799 on 2018/7/9.
 */

public class BannerUtil {

    public static void showBanner(ConvenientBanner convenientBanner, List<String> bannerlist) {
        List<String> bannerStr = new ArrayList<>();
        for (String vm : bannerlist) {
            bannerStr.add(vm);
        }
       // convenientBanner.startTurning(3000);
        convenientBanner.stopTurning();
        convenientBanner.setPages(new CBViewHolderCreator<NetworkImageHolderView>() {
            @Override
            public NetworkImageHolderView createHolder() {
                return new NetworkImageHolderView();
            }
        }, Arrays.asList(bannerStr.toArray()))
//                .setPageIndicator(new int[]{R.drawable.dot, R.drawable.dot_selected})
                .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
    }
}
